## Installation

install node_modules

```bash
npm i
```

## Usage

```bash

# Run in production mode
npm run production

# Run in develop mode
npm run dev

#Running Stored Procedure from a ‘.sql’ file in :
modulesName/model/tsql/FilesName.sql
```
