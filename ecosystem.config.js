module.exports = {
  apps: [
    {
      name: "event",
      // eslint-disable-next-line camelcase
      exec_mode: "cluster",
      instances: "max", // Or a number of instances
      script: "./src/server.js",
      args: "production",
    },
  ],
};
