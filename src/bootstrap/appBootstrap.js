const bodyParser = require("body-parser");
const path = require("path");
const dir = require("node-dir");
const {
  loadHelper,
  loadConfig,
  controller,
  loadModels,
  validators,
  middleware,
} = require("./../loaders/loader");

module.exports = (express, app) => {
  return {
    async appSetup() {
      //globally availability of loaders
      if (!global.loadHelper) {
        global.loadHelper = loadHelper;
      }
      if (!global.loadConfig) {
        global.loadConfig = loadConfig;
      }
      if (!global.Model) {
        global.Model = loadModels();
      }

      global.Controller = require("./../core/controller");
      global.Transform = require("./../core/Transform");

      // database connection initialize
      let db = loadConfig("dbConfig");
      let { sqlConfig } = loadConfig("appConfig");
      await db.createConnection(sqlConfig);
      if (db.getConnection()) {
        // eslint-disable-next-line require-atomic-updates
        global.sql = await db.getConnection();
      }
    },

    startRouting() {
      app.use(bodyParser.json());
      app.use(bodyParser.urlencoded({ extended: true }));

      /* app.use(express.json());
         app.use(express.urlencoded({ extended: false })); */

      const routerProperties = {
        express: express,
        middleware,
        validators,
        controller,
      };
      let isProduction = process.env.npm_lifecycle_event === "production";
      let files = dir.files("./src/modules", { sync: true });
      files.map((file) => {
        let split = file.split("/");
        if (split.slice(-1)[0] == "router.js") {
          if (!isProduction) {
            let router = require(path.resolve(file));
            app.use("/", router(routerProperties));
          } else if (!split.includes(".example")) {
            let router = require(path.resolve(file));
            app.use("/", router(routerProperties));
          }
        }
      });
    },
  };
};
