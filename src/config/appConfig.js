module.exports = {
  appName: process.env.APP_NAME || "example app name",
  requestLogIntoFile: process.env.REQUEST_LOG_INTO_FILE || false,
  sessionSecret: process.env.SESSION_SECRET || "4s6zhdjnst",
  sessionName: process.env.SESSION_NAME || "",
  sessionExpired: process.env.SESSION_EXPIRED || 7 * 24 * 60 * 60 * 1000, //7 day default
  jwtSecret: process.env.JWT_SECRET || "examplesecret",
  sendGridApiKey: process.env.SENDGRID_API_KEY || "",
  mailFrom: process.env.MAIL_FROM || "",
  sqlConfig: {
    user: process.env.DB_USER,
    password: process.env.DB_PWD,
    database: process.env.DB_NAME,
    server: process.env.DB_HOST,
    pool: {
      max: 10,
      min: 0,
      idleTimeoutMillis: 30000,
    },
    options: {
      //   encrypt: true, // for azure
      trustServerCertificate: true, // change to true for local dev / self-signed certs
    },
  },
};
