const sql = require("mssql");

let connection = null;

async function createConnection(config) {
  connection = await new sql.ConnectionPool(config)
    .connect()
    .then((pool) => {
      console.log("mssql Connected...");
      return pool;
    })
    .catch((error) => console.log("mssql connection failed", error));
}

async function getConnection() {
  return connection;
}

module.exports = { createConnection, getConnection };
