module.exports = class Transform {
  transformCollection(items, page, limit) {
    if (this.withPaginate) {
      return {
        [this.CollectionName()]: items.recordset.map(this.transform),
        ...this.paginateItem(items, page, limit),
      };
    }
    return items.map(this.transform);
  }

  paginateItem(items, page, limit) {
    return {
      total: items.recordsets[1][0].count,
      limit: limit,
      page: page,
      pages: Math.ceil(items.recordsets[1][0].count / limit),
    };
  }

  CollectionName(CollectName = "items") {
    return CollectName;
  }

  withPaginate() {
    this.withPaginate = true;
    return this;
  }
};
