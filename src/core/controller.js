const { validationResult } = require("express-validator");

module.exports = class Controller {
  async validationData(req, res) {
    const result = validationResult(req);
    if (!result.isEmpty()) {
      const errors = result.array();
      const messages = [];
      errors.forEach((err) => {
        delete err.location;
        delete err.value;
        messages.push(err);
      });
      req.errors = messages;
      await res
        .json({
          ok: false,
          errors: messages,
          code: 400,
          status: "error",
        })
        .end();

      return false;
    }

    return true;
  }

  successResponse(res, payload, statusCode = 200) {
    return res.status(statusCode).json({
      ok: true,
      code: statusCode,
      data: payload,
      status: "success",
    });
  }

  // eslint-disable-next-line max-params
  errorResponse(res, err, param, msg, statusCode = 400) {
    if (process.env.npm_lifecycle_event == "dev") {
      console.log(err);
    }
    return res.status(statusCode).json({
      ok: false,
      code: statusCode,
      errors: [{ param, msg }],
      status: "error",
    });
  }
};
