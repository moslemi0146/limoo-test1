exports.isNumber = (value) => {
  return !isNaN(parseFloat(value)) && isFinite(value);
};
