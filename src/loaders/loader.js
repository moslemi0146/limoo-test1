const path = require("path");
const dir = require("node-dir");

const controller = (moduleDir, controllerPath, ...options) => {
  let split = controllerPath.split(".");
  let getPath = controllerPath.replace("." + split[split.length - 1], "");
  let Controller = require(path.join(moduleDir, "controllers", getPath));
  let controller = new Controller(...options);
  let methodName = split[split.length - 1];
  let method = controller[methodName];
  if (!method) throw new Error(`${methodName} does not exit on the controller`);
  return method.bind(controller);
};

const validators = (moduleDir, validatorsPath) => {
  let split = validatorsPath.split(".");
  let getPath = validatorsPath.replace("." + split[split.length - 1], "");
  let Validators = require(path.join(moduleDir, "validators", getPath));
  return Validators;
};

const middleware = (moduleDir, middlewarePath) => {
  let split = middlewarePath.split(".");
  let getPath = middlewarePath.replace("." + split[split.length - 1], "");
  let Middleware = require(path.join(moduleDir, "middleware", getPath));
  return Middleware;
};

const loadModels = () => {
  const models = {};
  let isProduction = process.env.npm_lifecycle_event === "production";
  let files = dir.files("./src/modules", { sync: true });
  files.map((file) => {
    let split = file.split("/");
    if (split.slice(-2)[0] == "model") {
      if (!isProduction) {
        let model = require(path.resolve(file));
        models[model.name] = model;
      } else if (!split.includes(".example")) {
        let model = require(path.resolve(file));
        models[model.name] = model;
      }
    }
  });
  return models;
};

const loadHelper = (name) => {
  const helperPath = path.join(__dirname, "..", "helpers", name);
  return require(helperPath);
};

const loadConfig = (name) => {
  const modulePath = path.join(__dirname, "..", "config", name);
  return require(modulePath);
};

module.exports = {
  loadHelper,
  loadConfig,
  controller,
  loadModels,
  validators,
  middleware,
};
