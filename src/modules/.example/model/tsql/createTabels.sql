USE [TestDB];
IF OBJECT_ID('user', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[user]
        (
            ID INT PRIMARY KEY
        );
    END
ELSE
    BEGIN
      DROP TABLE [dbo].[user]
      CREATE TABLE [dbo].[user]
      (
          ID INT PRIMARY KEY,
          Name VARCHAR(30)
      );
    END
GO