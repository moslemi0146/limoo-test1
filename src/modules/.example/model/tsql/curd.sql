USE [TestDB]

-- start add user PROCEDURE --
DROP PROC IF EXISTS [dbo].[AddUser]
GO

CREATE PROCEDURE [dbo].[AddUser] @Name VARCHAR(30) AS
    IF EXISTS(SELECT 1
              FROM [user]
              WHERE Name = @Name)
        BEGIN
            UPDATE [user] SET Name = @Name WHERE Name = @Name
        END
    ELSE
        BEGIN
            INSERT INTO [user] (Name , ID) VALUES (@Name,1)
        END

GO
-- end add user PROCEDURE --