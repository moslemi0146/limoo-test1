module.exports = ({ express, controller }) => {
  const router = express.Router();

  router.get("/test/create", controller(__dirname, "exampleController.create"));
  router.get("/test/create", controller(__dirname, "exampleController.store"));
  return router;
};
