const UserTransform = require(`../transforms/UserTransform`);
module.exports = class UserController extends Controller {
  async index(req, res) {
    let page = req.query.page || req.body.page || 1;
    let limit = req.query.limit || req.body.limit || 8;
    try {
      let results = await Model.User.GetAllUsers(page, limit);
      this.successResponse(
        res,
        new UserTransform().transformCollection(results, page, limit)
      );
    } catch (err) {
      this.errorResponse(res, err, "user", "get users error");
    }
  }

  async create(req, res) {
    const valid = await this.validationData(req, res);
    if (!valid) return;
    try {
      await Model.User.AddUser(req.body);
      this.successResponse(res);
    } catch (err) {
      this.errorResponse(res, err, "user", "create user error");
    }
  }

  async getCount(req, res) {
    try {
      let result = await Model.User.GetUserCount();
      this.successResponse(res, result);
    } catch (err) {
      this.errorResponse(res, err, "user", "error get count");
    }
  }

  async addView(req, res) {
    try {
      await Model.View.addView();
      this.successResponse(res);
    } catch (err) {
      this.errorResponse(res, err, "view", "add view error");
    }
  }

  async getViewCount(req, res) {
    try {
      let result = await Model.View.getViewCount();
      this.successResponse(res, result);
    } catch (err) {
      this.errorResponse(res, err, "view", "add view error");
    }
  }

  async search(req, res) {
    let page = req.query.page || req.body.page || 1;
    let limit = req.query.limit || req.body.limit || 8;
    let term = req.query.term || req.body.term || "";
    try {
      let results = await Model.User.SearchUser(page, limit, term);
      this.successResponse(
        res,
        new UserTransform().transformCollection(results, page, limit)
      );
    } catch (err) {
      this.errorResponse(res, err, "user", "search user error");
    }
  }

  async delete(req, res) {
    const valid = await this.validationData(req, res);
    if (!valid) return;
    try {
      await Model.User.DeleteUser(req.body.uuid);
      this.successResponse(res);
    } catch (err) {
      this.errorResponse(res, err, "user", "delete user error");
    }
  }
};
