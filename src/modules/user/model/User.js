const { v4: uuidv4 } = require("uuid");
module.exports = class User {
  static async AddUser({ fullName, phone, email, message }) {
    try {
      return await sql
        .request()
        .input("FullName", fullName)
        .input("Uuid", uuidv4())
        .input("Phone", phone)
        .input("Email", email)
        .input("Message", message)
        .execute("dbo.AddUser");
    } catch (error) {
      throw new Error(error);
    }
  }

  static async FindUserByPhone(value) {
    try {
      let result = await sql
        .request()
        .input("Value", value)
        .execute("dbo.FindUserByPhone");
      return result.recordset.length > 0 ? result.recordset[0] : null;
    } catch (error) {
      throw new Error(error);
    }
  }

  static async FindUserByEmail(value) {
    try {
      let result = await sql
        .request()
        .input("Value", value)
        .execute("dbo.FindUserByEmail");
      return result.recordset.length > 0 ? result.recordset[0] : null;
    } catch (error) {
      throw new Error(error);
    }
  }

  static async GetUserCount() {
    try {
      let result = await sql.request().execute("dbo.GetUserCount");
      return result.recordset.length > 0 ? result.recordset[0] : null;
    } catch (error) {
      throw new Error(error);
    }
  }

  static async GetAllUsers(page, limit) {
    try {
      let result = await sql
        .request()
        .input("Page", page)
        .input("Limit", limit)
        .execute("dbo.GetAllUsers");
      return result;
    } catch (error) {
      throw new Error(error);
    }
  }

  static async SearchUser(page, limit, term) {
    try {
      let result = await sql
        .request()
        .input("Term", term)
        .input("Page", page)
        .input("Limit", limit)
        .execute("dbo.SearchUser");
      return result;
    } catch (error) {
      throw new Error(error);
    }
  }

  static async DeleteUser(uuid) {
    try {
      await sql.request().input("Uuid", uuid).execute("dbo.DeleteUser");
    } catch (error) {
      throw new Error(error);
    }
  }
};
