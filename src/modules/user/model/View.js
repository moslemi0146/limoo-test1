module.exports = class View {
  static async addView() {
    try {
      await sql.request().execute("dbo.addView");
    } catch (error) {
      throw new Error(error);
    }
  }

  static async getViewCount() {
    try {
      let result = await sql.request().execute("dbo.getViewCount");
      return result.recordset.length > 0 ? result.recordset[0] : null;
    } catch (error) {
      throw new Error(error);
    }
  }
};
