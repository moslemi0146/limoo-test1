USE [TestDB];

IF OBJECT_ID('user', 'U') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[user]
    END
IF OBJECT_ID('user', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[user]
        (
            ID        INT PRIMARY KEY IDENTITY(1,1),
            Uuid      VARCHAR(255) NOT NULL,
            FullName  VARCHAR(255),
            Phone     VARCHAR(11) UNIQUE,
            Email     VARCHAR(255) UNIQUE,
            Message   VARCHAR(max),
            Status    TINYINT DEFAULT 1,
            CreatedAt DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
            UpdatedAt DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
        );
    END
GO

IF OBJECT_ID('view', 'U') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[view]
    END
IF OBJECT_ID('view', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[view]
        (
            ID    INT PRIMARY KEY IDENTITY(1,1),
            CreatedAt DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
        );
    END
GO