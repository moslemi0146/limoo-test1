USE [TestDB]
-- START ADD USER PROCEDURE --
DROP PROC IF EXISTS [dbo].[AddUser]
GO
CREATE PROCEDURE [dbo].[AddUser] @Uuid VARCHAR(255), @FullName VARCHAR(255), @Phone VARCHAR(11), @Email VARCHAR(255),
                                 @Message VARCHAR(MAX) AS
    IF NOT EXISTS(SELECT 1
                  FROM [user]
                  WHERE (Phone = @Phone OR Email = @Email))
        BEGIN
            INSERT INTO [user] (Uuid, FullName, Phone, Email, Message, Status)
            VALUES (@Uuid, @FullName, @Phone, @Email, @Message, 1)
        END
GO
-- END ADD USER PROCEDURE --

-- START FIND USER BY PHONE PROCEDURE --
DROP PROC IF EXISTS [dbo].[FindUserByPhone]
GO
CREATE PROCEDURE [dbo].[FindUserByPhone] @Value VARCHAR(255) AS
SELECT *
FROM [user]
WHERE Phone = @Value
GO
-- END FIND USER BY PHONE PROCEDURE --


-- START FIND USER BY EMAIL PROCEDURE --
DROP PROC IF EXISTS [dbo].[FindUserByEmail]
GO
CREATE PROCEDURE [dbo].[FindUserByEmail] @Value VARCHAR(255) AS
SELECT *
FROM [user]
WHERE Email = @Value
GO
-- END FIND USER BY EMAIL PROCEDURE --

-- START GET USER COUNT PROCEDURE --
DROP PROC IF EXISTS [dbo].[GetUserCount]
GO
CREATE PROCEDURE [dbo].[GetUserCount] AS
SELECT COUNT(*) AS count
FROM [user]
WHERE Status = 1
GO
-- END GET USER COUNT PROCEDURE --

-- START GET ALL USER PROCEDURE --
DROP PROC IF EXISTS [dbo].[GetAllUsers]
GO
CREATE PROCEDURE [dbo].[GetAllUsers] @Page INT, @Limit INT AS

SELECT *
FROM (
         SELECT *, ROW_NUMBER() OVER (ORDER BY ID DESC ) as row
         FROM [user]
     ) a
WHERE (a.row > (@Page - 1) * @Limit and a.row <= (@Page - 1) * @Limit + @Limit)
  AND Status = 1

SELECT COUNT(*) AS count
FROM [user]
GO
-- END GET ALL USER PROCEDURE --

-- START SEARCH USER PROCEDURE --
DROP PROC IF EXISTS [dbo].[SearchUser]
GO
CREATE PROCEDURE [dbo].[SearchUser] @Page INT, @Limit INT, @TERM VARCHAR(255) AS

SELECT *
FROM (
         SELECT *, ROW_NUMBER() OVER (ORDER BY ID DESC ) as row
         FROM [user]
     ) a
WHERE (a.row > (@Page - 1) * @Limit and a.row <= (@Page - 1) * @Limit + @Limit)
  AND (FullName LIKE '%' + @TERM + '%' OR Phone LIKE '%' + @TERM + '%')
  AND Status = 1

SELECT COUNT(*) AS count
FROM [user]
GO
-- END SEARCH USER PROCEDURE --

-- START DELETE USER PROCEDURE --
DROP PROC IF EXISTS [dbo].[DeleteUser]
GO
CREATE PROCEDURE [dbo].[DeleteUser] @Uuid VARCHAR(255) AS
UPDATE [user]
SET Status = 2
WHERE Uuid = @Uuid
GO
-- END DELETE USER PROCEDURE --


-- START ADD VIEW PROCEDURE --
DROP PROC IF EXISTS [dbo].[addView]
GO
CREATE PROCEDURE [dbo].[addView] AS
INSERT INTO [view] (CreatedAt) VALUES (CURRENT_TIMESTAMP)
GO
-- END ADD VIEW PROCEDURE --

-- START GET VIEW COUNT PROCEDURE --
DROP PROC IF EXISTS [dbo].[getViewCount]
GO
CREATE PROCEDURE [dbo].[getViewCount] AS
SELECT COUNT(*) AS count
FROM [view] WHERE CreatedAt >= DATEADD(DAY ,-1,CreatedAt)
GO
-- END GET VIEW COUNT PROCEDURE --