module.exports = ({ express, controller, validators }) => {
  const router = express.Router();

  //menu1
  router.post(
    "/user/create",
    validators(__dirname, "CreateUserValidator"),
    controller(__dirname, "UserController.create")
  );
  router.post("/user/count", controller(__dirname, "UserController.getCount"));
  router.get("/addView", controller(__dirname, "UserController.addView"));
  router.get(
    "/getViewCount",
    controller(__dirname, "UserController.getViewCount")
  );

  //menu2
  router.get("/user/all", controller(__dirname, "UserController.index"));
  router.get("/user/search", controller(__dirname, "UserController.search"));
  router.post(
    "/user/delete",
    validators(__dirname, "deleteUserValidator"),
    controller(__dirname, "UserController.delete")
  );

  return router;
};
