const moment = require("jalali-moment");
moment().locale("fa").format("YYYY/M/D");

// eslint-disable-next-line no-undef
module.exports = class UserTransform extends Transform {
  transform(item) {
    return {
      uuid: item.Uuid,
      fullName: item.FullName,
      phone: item.Phone,
      email: item.Email,
      message: item.Message,
      status: item.Status,
      createdAt: moment(item.CreatedAt).format("jYYYY/jM/jD HH:mm:ss"),
      updatedAt: moment(item.UpdatedAt).format("jYYYY/jM/jD HH:mm:ss"),
    };
  }
};
