const { body } = require("express-validator");

module.exports = (() => {
  return [
    body("fullName")
      .notEmpty()
      .withMessage("فیلد نام و نام خوانوادگی الزامیست!"),

    body("phone").custom(async (value, { req }) => {
      if (!value || !value.match(/^(\+98|0)?9\d{9}$/gi)) {
        return Promise.reject("شماره تماس وارد شده معتبر نیست!");
      }
      if (await Model.User.FindUserByPhone(req.body.phone)) {
        return Promise.reject("شماره تماس وارد شده قبلا ثبت شده است!");
      }
    }),
    body("email")
      .isEmail()
      .withMessage("ایمیل وارد شده معتبر نیست!")
      .custom(async (value, { req }) => {
        if (await Model.User.FindUserByEmail(req.body.email)) {
          return Promise.reject("ایمیل وارد شده قبلا ثبت شده است!");
        }
      }),
    body("message").notEmpty().withMessage("فیلد پیام الزامیست!"),
  ];
})();
