const { body } = require("express-validator");
const { validate } = require("uuid");
module.exports = (() => {
  return [
    body("uuid")
      .notEmpty()
      .withMessage("شناسه وارد شده معتبر نیست!")
      .custom(async (value, { req }) => {
        if (!validate(req.body.uuid)) {
          return Promise.reject("شناسه وارد شده معتبر نیست!");
        }
      }),
  ];
})();
