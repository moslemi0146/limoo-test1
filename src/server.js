const express = require("express");
require("dotenv").config();
const app = express();
const bootstraping = require("./bootstrap/appBootstrap")(express, app);

// application start with setup
bootstraping.appSetup();
//start routing
bootstraping.startRouting();
const PORT = process.env.APP_PORT || 4646;
app.listen(PORT, async () => {
  // eslint-disable-next-line no-console
  console.log("Server running at port %d", PORT);
});
